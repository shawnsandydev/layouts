<?php

namespace ShawnSandy\Layouts;


use Illuminate\Support\ServiceProvider;

class LayoutsServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Register views
         */
        $this->loadViewsFrom(__DIR__ . '/views', 'layouts');

        /*
         * Publish Views
         */
        $this->publishes([
            //views
            __DIR__ . '/views' => resource_path('views/layouts')
        ], 'views');

        /*
         * Publish config
         */
        $this->publishes([
            __DIR__ . '/config' => config_path('layout.php')
        ], 'config');

        /*
         * Assets
         */
        $this->publishes([
            __DIR__ . '/assets' => public_path('layouts')
        ], 'public');


    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Bind the facade to to the class
         */
        $this->app->bind('layouts', function () {
            return new \ShawnSandy\Layouts\Layouts;
        });
    }
}
