<?php

namespace ShawnSandy\Layouts;
use Illuminate\Support\Facades\Facade;

/**
 * Class PackageFacade
 *
 * @package \ShawnSandy\LaravelStarter
 */

class LayoutsFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        /*
         * The service container
         */
       return 'layouts';
    }

}
