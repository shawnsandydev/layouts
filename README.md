# Layouts

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Total Downloads][ico-downloads]][link-downloads]
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a1dbd63a-f7b4-46ea-a918-1cf1d190bc82/mini.png)](https://insight.sensiolabs.com/projects/a1dbd63a-f7b4-46ea-a918-1cf1d190bc82)

Laravel layout/view toolkit

## Install

Via Composer

``` bash
$ composer require shawnsandy/FolukeLayout
```

## Usage

``` php
$skeleton = new League\Skeleton();
echo $skeleton->echoPhrase('Hello, League!');
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email shawnsandy04@gmail.com instead of using the issue tracker.

## Credits

- [Shawn Sandy][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/shawnsandy/FolukeLayout.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/shawnsandy/FolukeLayout/master.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/shawnsandy/FolukeLayout.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/shawnsandy/FolukeLayout
[link-travis]: https://travis-ci.org/shawnsandy/FolukeLayout
[link-downloads]: https://packagist.org/packages/shawnsandy/FolukeLayout
[link-author]: https://github.com/shawnsandy
[link-contributors]: ../../contributors
